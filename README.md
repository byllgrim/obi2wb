OBI2WB is an adapter module that translates from the [OBI][obi]
bus protocol to the [WISHBONE][wb] bus protocol.

[obi]: https://github.com/openhwgroup/core-v-docs/tree/master/cores/obi
[wb]: https://opencores.org/howto/wishbone

WORK IN PROGRESS!
