Here are files borrowed from cv32e40p by OpenHW Group.

A memory module is useful for verifying the OBI side of obi2wb.


## OBI

Referring to the OBI-v1.0.pdf spec.

Global
* clk
* reset_n

A-channel
* req
* gnt
* addr
* we
* be
* wdata
* auser
* wuser
* aid

R-channel
* rvalid
* rready
* rdata
* err
* ruser
* rid
