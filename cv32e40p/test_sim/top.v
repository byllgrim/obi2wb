module top;
  reg sys_v_clk;
  reg sys_v_rst;

  reg        data_v_req;
  reg [31:0] data_v_addr;
  reg        data_v_we;
  reg [ 3:0] data_v_be;
  reg [31:0] data_v_wdata;
  wire data_w_rvalid;
  wire data_w_gnt;

  mm_ram #(
    .RAM_ADDR_WIDTH(8),
    .INSTR_RDATA_WIDTH(32)
  ) ram (
    .clk_i     (sys_v_clk),
    .rst_ni    (~sys_v_rst),

    .instr_req_i('0),
    .instr_addr_i(8'd0),
    .instr_rdata_o(),
    .instr_rvalid_o(),
    .instr_gnt_o(),

    .data_req_i    (data_v_req),
    .data_addr_i   (data_v_addr),
    .data_we_i     (data_v_we),
    .data_be_i     (data_v_be),
    .data_wdata_i  (data_v_wdata),
    .data_rdata_o  (),
    .data_rvalid_o (data_w_rvalid),
    .data_gnt_o    (data_w_gnt),
    .data_atop_i   (6'd0),

    .irq_id_i(5'd0),
    .irq_ack_i('0),

    .irq_software_o(),
    .irq_timer_o(),
    .irq_external_o(),
    .irq_fast_o(),

    .pc_core_id_i(32'd0),

    .tests_passed_o(),
    .tests_failed_o(),
    .exit_valid_o(),
    .exit_value_o()
    );


  // Dump vcd
  initial begin
    $dumpfile("trace.vcd");
    $dumpvars();
  end

  // Clock
  initial begin
    sys_v_clk = 0;
    forever #5 sys_v_clk = !sys_v_clk;
  end

  // Reset
  initial begin
    sys_v_rst = 0;
    #1;
    sys_v_rst = 1;
    #24;
    sys_v_rst = 0;
  end

  // Main test
  initial begin
    @(posedge sys_v_rst);
    clear_data_signals();
    @(negedge sys_v_rst);

    set_write_request(32'hAABBCCDD, 'h10);
    perform_transaction;

    @(posedge sys_v_clk);
    set_write_request(32'h11223344, 'h20);
    perform_transaction;

    repeat (2) @(posedge sys_v_clk);
    set_read_request('h10);
    perform_transaction;

    @(posedge sys_v_clk);
    set_read_request('h20);
    perform_transaction;

    repeat (32) @(posedge sys_v_clk);
    $finish;
  end


  // Helper tasks

  task clear_data_signals;
    data_v_req = 0;
    data_v_addr = 0;
    data_v_be = 0;
    data_v_we = 0;
    data_v_wdata = 0;
  endtask

  task set_read_request(input [31:0] addr);
    data_v_req = 1;
    data_v_addr = addr;
    data_v_be = 4'b1111;
    data_v_we = 0;
    data_v_wdata = 0;
  endtask

  task set_write_request(input [31:0] wdata, [31:0] addr);
    set_read_request(addr);
    data_v_we = 1;
    data_v_wdata = wdata;
  endtask

  task perform_transaction;
    @(posedge sys_v_clk);
    #1;  // TODO should not be necessary!
    while (!data_w_gnt) @(posedge sys_v_clk);
    clear_data_signals();
  endtask

endmodule
