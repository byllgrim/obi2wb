// This testbench uses vip_obi to write/read mm_ram

`default_nettype none

module top2;

  reg         sys_v_clk;
  reg         sys_v_rst;

  reg         ctl_v_wr;
  reg  [31:0] ctl_v_data;
  reg  [31:0] ctl_v_addr;

  wire        obi_w_req;
  wire [31:0] obi_w_addr;
  wire        obi_w_we;
  wire [ 3:0] obi_w_be;
  wire [31:0] obi_w_wdata;

  mm_ram #(
      .RAM_ADDR_WIDTH   (12),
      .INSTR_RDATA_WIDTH(32)
  ) ram (
      .clk_i (sys_v_clk),
      .rst_ni(~sys_v_rst),

      .instr_req_i ('0),
      .instr_addr_i(12'd0),

      .data_req_i  (obi_w_we),
      .data_addr_i (obi_w_addr),
      .data_we_i   (obi_w_we),
      .data_be_i   (obi_w_be),
      .data_wdata_i(obi_w_wdata),
      .data_atop_i (6'd0),

      .irq_id_i (5'd0),
      .irq_ack_i('0),

      .pc_core_id_i(32'd0)
  );

  vip_obi obi (
      .sys_i_clk(sys_v_clk),
      .sys_i_rst(sys_v_rst),

      .ctl_i_wr  (ctl_v_wr),
      .ctl_i_data(ctl_v_data),
      .ctl_i_addr(ctl_v_addr),

      .obi_o_req   (obi_w_req),
      .obi_o_addr(obi_w_addr),
      .obi_o_we(obi_w_we),
      .obi_o_be(obi_w_be),
      .obi_o_wdata(obi_w_wdata)
  );

  initial begin
    sys_v_clk = 1;
    forever #5 sys_v_clk = ~sys_v_clk;
  end

  initial begin
    $dumpfile("trace.vcd");
    $dumpvars();

    sys_v_rst = 0;
    #1;
    sys_v_rst = 1;
    repeat (2) @(posedge sys_v_clk);
    #1 sys_v_rst = 0;

    repeat (16) @(posedge sys_v_clk);
    $finish;
  end

endmodule
