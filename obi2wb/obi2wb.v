`default_nettype none

module obi2wb (
    input wire sys_i_clk,
    input wire sys_i_rst,

    input  wire        ach_i_req,
    input  wire        ach_i_we,
    input  wire [ 3:0] ach_i_be,
    input  wire [31:0] ach_i_addr,
    input  wire [31:0] ach_i_wdata,
    output wire        ach_o_gnt,

    output wire        rch_o_rvalid,
    output wire [31:0] rch_o_rdata,

    input  wire [31:0] wb_i_dat,
    output wire [31:0] wb_o_dat,

    input  wire        wbm_i_ack,
    input  wire        wbm_i_err,
    input  wire        wbm_i_stall,
    output wire        wbm_o_cyc,
    output wire        wbm_o_stb,
    output wire        wbm_o_we,
    output wire [ 3:0] wbm_o_sel,
    output wire [31:0] wbm_o_adr
);

  reg aux_r1_err;


  assign ach_o_gnt = ~wbm_i_stall;

  assign rch_o_rvalid = wbm_i_ack;
  assign rch_o_rdata = 0;

  assign wb_o_dat = ach_i_wdata;

  assign wbm_o_cyc = ach_i_req && ~aux_r1_err;
  assign wbm_o_stb = ach_i_req && wbm_o_cyc;
  assign wbm_o_we = ach_i_we;
  assign wbm_o_sel = ach_i_be;
  assign wbm_o_adr = ach_i_addr;


  always @(posedge sys_i_clk) begin
    aux_r1_err <= wbm_i_err;
  end


`ifdef FORMAL

  initial assume(sys_i_rst);

  fwb_master fwb(
    .i_clk (sys_i_clk),
    .i_reset(sys_i_rst),

    .i_wb_cyc (wbm_o_cyc),
    .i_wb_stb (wbm_o_stb),
    .i_wb_addr (wbm_o_adr),
    .i_wb_sel (wbm_o_sel),
    .i_wb_we (wbm_o_we),
    .i_wb_data (wb_o_dat),

    .i_wb_err (wbm_i_err),
    .i_wb_stall (wbm_i_stall)
  );

  obi_formal fobi(
    .clk(sys_i_clk),
    .rst(sys_i_rst),

    .req(ach_i_req),
    .addr(ach_i_addr),
    .be(ach_i_be),
    .we(ach_i_we),
    .wdata(ach_i_wdata),

    .gnt(ach_o_gnt)
  );

`endif

endmodule
