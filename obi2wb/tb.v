`default_nettype none

module tb;

  reg         sys_v_clk;
  reg         sys_v_rst;

  reg         obi_v_req;
  reg         obi_v_we;
  reg  [ 3:0] obi_v_be;
  reg  [31:0] obi_v_addr;
  reg  [31:0] obi_v_wdata;

  wire obi_w_gnt;

  wire        wbm_w_cyc;
  wire        wbm_w_stb;
  wire        wbm_w_we;
  wire [ 3:0] wbm_w_sel;
  wire [31:0] wbm_w_adr;
  wire [31:0] wbm_w_dat;

  wire        wbs_w_ack;
  wire        wbs_w_stall;
  wire [31:0] wbs_w_dat;

  obi2wb obi2wb (
      .sys_i_clk(sys_v_clk),
      .sys_i_rst(sys_v_rst),

      .ach_i_req(obi_v_req),
      .ach_i_addr(obi_v_addr),
      .ach_i_be(obi_v_be),
      .ach_i_we(obi_v_we),
      .ach_i_wdata(obi_v_wdata),
      .ach_o_gnt(obi_w_gnt),

      .wbm_i_ack(wbs_w_ack),
      .wbm_i_err(1'd0),
      .wbm_i_stall(wbs_w_stall),
      .wbm_o_cyc(wbm_w_cyc),
      .wbm_o_stb(wbm_w_stb),
      .wbm_o_we(wbm_w_we),
      .wbm_o_sel(wbm_w_sel),
      .wbm_o_adr(wbm_w_adr),

      .wb_o_dat(wbm_w_dat),
      .wb_i_dat(wbs_w_dat)
  );

  memdev memdev (
      .i_clk  (sys_v_clk),
      .i_reset(sys_v_rst),

      .i_wb_cyc (wbm_w_cyc),
      .i_wb_stb (wbm_w_stb),
      .i_wb_we  (wbm_w_we),
      .i_wb_addr(wbm_w_adr[12:0]),
      .i_wb_data(wbm_w_dat),
      .i_wb_sel (wbm_w_sel),

      .o_wb_ack  (wbs_w_ack),
      .o_wb_stall(wbs_w_stall),
      .o_wb_data (wbs_w_dat)
  );

  initial begin
    $dumpfile("trace.vcd");
    $dumpvars();
    #10;
  end

  initial begin
    sys_v_clk = 0;
    forever #5 sys_v_clk = !sys_v_clk;
  end

  initial begin
    sys_v_rst = 0;
    #1 sys_v_rst = 1;
    repeat (2) @(posedge sys_v_clk);
    #1 sys_v_rst = 0;
  end

  initial begin
    while (!sys_v_rst) #1;
    clear_obi_signals();
    @(negedge sys_v_rst);
    @(posedge sys_v_clk);
    #1;

    initiate_write('h4, 'hABCD_ABCD);
    finish_transaction();
    initiate_write('h8, 'hFFFF_FFFF);
    finish_transaction();

    initiate_read('h4);
    finish_transaction();
    initiate_read('h8);
    finish_transaction();

    repeat (8) @(posedge sys_v_clk);
    $finish();
  end

  task clear_obi_signals; begin
    obi_v_req = 0;
    obi_v_we = 0;
    obi_v_be = 0;
    obi_v_addr = 0;
    obi_v_wdata = 0;
  end endtask

  task initiate_write(input [31:0] addr, input [31:0] data); begin
    obi_v_req = 1;
    obi_v_be = -1;
    obi_v_we = 1;

    obi_v_addr = addr;
    obi_v_wdata = data;
  end endtask

  task initiate_read(input [31:0] addr); begin
    obi_v_req = 1;
    obi_v_be = -1;
    obi_v_we = 0;
    obi_v_wdata = 0;

    obi_v_addr = addr;
  end endtask

  task finish_transaction; begin
    @(posedge sys_v_clk);
    while (!obi_w_gnt) @(posedge sys_v_clk);
    #1;
    clear_obi_signals();
  end endtask

endmodule
