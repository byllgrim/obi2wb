`default_nettype none

module obi_formal(
  input wire clk,
  input wire rst,

  input wire req,
  input wire we,
  input wire [ 3:0] be,
  input wire [31:0] addr,
  input wire [31:0] wdata,

  input wire gnt
);

  always @(posedge clk) begin
    if (rst || $past(rst)) begin
      assume(req == 0);
    end else begin
      // OBI-v1.2 R-3.1.1
      // The master shall keep its address phase signals stable during the address phase
      if (req && $past(req && !gnt)) begin
        assume(addr == $past(addr));
        assume(be == $past(be));
        assume(we == $past(we));
        assume(wdata == $past(wdata));
      end
    end
  end

endmodule
