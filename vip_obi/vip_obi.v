`default_nettype none

module vip_obi (
  input wire sys_i_clk,
  input wire sys_i_rst,

  input  wire        ctl_i_wr,
  input  wire [31:0] ctl_i_data,
  input  wire [31:0] ctl_i_addr,
  output reg         ctl_o_done,
  output reg  [31:0] ctl_o_data,

  output reg        obi_o_req,
  output reg        obi_o_we,
  output reg [ 3:0] obi_o_be,
  output reg [31:0] obi_o_addr,
  output reg [31:0] obi_o_wdata
);

  always @(posedge sys_i_clk) begin
    ctl_o_done <= 0;
    ctl_o_data <= 0;

    obi_o_req <= 0;
    obi_o_we <= 0;
    obi_o_be <= 0;
    obi_o_addr <= 0;
    obi_o_wdata <= 0;
  end

endmodule
