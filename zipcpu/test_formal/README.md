Here are formal tests for the WB memory module.

Since the memory module is going to be the authority on WB
for the OBI2WB verification,
the formal WB assertions try to check that it actually is
suitable for that job.
