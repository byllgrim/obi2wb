`timescale 1ns / 1ns
`default_nettype none

/* Legend:
 * (nb, this is experimental methodology)
 *
 * Groups: sys, wb
 *
 * Types:
 * w-ire.
 * v-ariable (blocking).
 * r-egister (clocked).
 * i-nput (wire).
 * o-utput (clocked register).
 */

module top;
  reg        sys_v_clk;
  reg        sys_v_rst;

  reg        wb_v_cyc;
  reg        wb_v_stb;
  reg        wb_v_we;
  reg [ 3:0] wb_v_sel;
  reg [31:0] wb_v_addr;
  reg [31:0] wb_v_data;


  memdev memdev (
      .i_clk  (sys_v_clk),
      .i_reset(sys_v_rst),

      .i_wb_cyc (wb_v_cyc),
      .i_wb_stb (wb_v_stb),
      .i_wb_we  (wb_v_we),
      .i_wb_addr(wb_v_addr[12:0]),
      .i_wb_data(wb_v_data),
      .i_wb_sel (wb_v_sel),

      .o_wb_ack  (),
      .o_wb_stall(),
      .o_wb_data ()
  );


  initial begin
    $dumpfile("trace.vcd");
    $dumpvars();
  end

  initial begin
    sys_v_clk = 0;
    forever #5 sys_v_clk = !sys_v_clk;
  end

  initial begin
    sys_v_rst = 0;
    #1;
    sys_v_rst = 1;
    repeat (2) @(posedge sys_v_clk);
    sys_v_rst = 0;
  end


  initial begin
    #3;
    clear_drivers();
    @(negedge sys_v_rst);

    @(posedge sys_v_clk);
    init_write(32'hAABBCCDD, 32'h10);
    perform_transaction();

    @(posedge sys_v_clk);
    init_write(32'h11223344, 32'h20);
    perform_transaction();

    @(posedge sys_v_clk);

    @(posedge sys_v_clk);
    init_transaction(32'h10);
    perform_transaction();

    @(posedge sys_v_clk);
    init_transaction(32'h20);
    perform_transaction();

    repeat (4) @(posedge sys_v_clk);
    $finish;
  end


  task clear_drivers;
    begin
      wb_v_cyc  = 0;
      wb_v_stb  = 0;
      wb_v_addr = 0;
      wb_v_data = 0;
      wb_v_we   = 0;
      wb_v_sel  = 0;
    end
  endtask

  task init_transaction(input [31:0] addr);
    begin
      wb_v_cyc  = 1;
      wb_v_stb  = 1;
      wb_v_sel  = 4'b1111;
      wb_v_addr = addr;
    end
  endtask

  task init_write(input [31:0] data, input [31:0] addr);
    begin
      wb_v_we   = 1;
      wb_v_data = data;
      init_transaction(addr);
    end
  endtask

  task perform_transaction;
    begin
      @(posedge sys_v_clk);
      #1;  // TODO should not be necessary
      clear_drivers();
      wb_v_cyc = 1;

      @(posedge sys_v_clk);
      clear_drivers();
    end
  endtask

endmodule
